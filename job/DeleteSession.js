const Loader = require(__dirname + "/../library/Loader.js").getLoader();
const Config = Loader.get("config").session;
const Cron = Loader.get("lib", "Cron");
const SessionManager = Loader.get("lib", "SessionManager");

function runJob(info){
	let ids = SessionManager.getAllIds();
	ids.forEach(id => {
		let Session = SessionManager.get(id);
		if (Session.lastUsed() < info.startTimeMs - Config.delete * 7 * 24 * 60 * 60 * 1000)
			Session.delete();
	})
}

Cron.register({
	name: "Delete Session",
	time: Cron.time.daily,
	handle: runJob
});