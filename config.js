module.exports = {
    log: {							                        // logger configuration
        folder: __dirname.replace(/\\/g, "/") + "/log/",		                    // path to the log folder
        fileSize: 1000000,				                    // fileSize in byte
        fileNumber: 6,				        	            // number of log files to keep
        level: "WARNING"				                        // logging level (possible: ERROR, WARNING, INFO, DEBUG, TRACE)
    },
    api: {
        port: 3001,                                         // webserver port
	    servicePath: __dirname.replace(/\\/g, "/") + "/service/"   // path where the services are stored
    },
    session: {
        location: __dirname.replace(/\\/g, "/") + "/session/",                  // directory for storing sessions
        contractLibrary: __dirname.replace(/\\/g, "/") + "/contract-library/",  // directory for storing sessions
		delete: 7                                                               // time in days after a an unused session should be deleted
    },
    cron: {
        path: __dirname.replace(/\\/g, "/") + "/job/",                          // directory for cron jobs
	    time: {                                                                 // pre defined times (accessible due Cron.time.XXX)
		    daily: "0 0 3 * * *",
		    weekly: "0 0 3 * * 0",
		    monthly: "0 0 3 1 * *",
		    yearly: "0 0 3 1 1 *"
	    }
    }
};