pragma solidity ^$COMPILER_VERSION;

import "./EIP/EIP165/EIP165_Default.sol";
$IMPORTS
contract $NAME is EIP165_Default$EXTENDS {

    constructor() $CONSTRUCTOR public {
        // Your own Smart Contract 😎
    }
}