const Loader = require(__dirname + "/Loader.js").getLoader();
const Logger = Loader.get("lib", "Logger");
const Config = Loader.get("config").session;
const fs = require('fs-extra');
const child_process = require('child_process');

exports.init = function(){
	return Generator;
};

/**
 * Checks if there is a matching constructor
 * @param config template.js config
 * @param variant variant config file
 * @returns matchingConstrutors
 */
function getMatchingConstructors(config, variant){
	let matchingConstructors = variant.constructor.filter(c => {
		if (c.parameters.length !== config.constructorVariable.length)
			return false;
		for (let i=0; i<c.parameters.length; ++i){
			if (c.parameters[i].type !== typeof config.constructorVariable[i])
				return false;
			if (c.parameters[i].higherequal && c.parameters[i].higherequal > config.constructorVariable[i])
				return false;
			if (c.parameters[i].higher && c.parameters[i].higher >= config.constructorVariable[i])
				return false;
			if (c.parameters[i].lower && c.parameters[i].lower <= config.constructorVariable[i])
				return false;
			if (c.parameters[i].lowerequal && c.parameters[i].lowerequal < config.constructorVariable[i])
				return false;
		}
		return true;
	});
	if (matchingConstructors.length <= 0){
		Logger.log(Logger.levels.WARNING, "There is no matching constructor for " + config.name + ":" + config.variant + " in the generatorConfig!", "Generator", config);
		throw new Error("There is no matching constructor for " + config.name + ": " + config.variant + " in the generatorConfig!");
	}
	return matchingConstructors;
}

class Generator{
	constructor(id){
		this.id = id;
		this.path = Config.location + id + "/";
		this.config = this.path + "generator.json";
		this.templates = this.path + "templates";
		this.contract = this.path + "contracts/Contract.sol";
		this.test = this.path + "test/Test.js";
		this.deploy = this.path + "migrations/2_deploy_contracts.js";
		this.oneFileSource = this.path + "Contract.flattened.sol";
	}

	/**
	 * Cleans the whole working directory
	 */
	clean(){
		let toDelete = [
			this.contract,
			this.test,
			this.deploy,
			this.oneFileSource,
			this.path + "/build/",
			this.path + "/node_modules/"
		];
		toDelete.forEach(d => fs.removeSync(d));
	}

	/**
	 * Generates the Contract.sol by using the generator.json
	 */
	generate(){
		this.clean();
		// import necessary files
		let config = JSON.parse(fs.readFileSync(this.config));
		let templates = require(this.templates);
		let contract = Loader.get("resource", "Contract");
		let Test = Loader.get("resource", "Test");
		let Deploy = Loader.get("resource", "Deploy");

		// check if name and templates are not empty
		if (config.name === undefined || config.name === ""){
			Logger.log(Logger.levels.WARNING, "Name must not be undefined in generatorConfig!", "Generator", config);
			throw new Error("Name must not be undefined in generatorConfig!");
		}
		if (config.templates === undefined || 0 === config.templates.length ){
			Logger.log(Logger.levels.WARNING, "At least one template need to be provided in the generatorConfig!", "Generator", config);
			throw new Error("At least one template need to be provided in the generatorConfig!");
		}

		let imports = "";
		let extend = "";
		let constructor = "";
		let test = "";
		config.templates.forEach(config => {
			// get variant and constructor variable for each template in generatorConfig
			let variant = templates.filter(t => t.name === config.name)[0]
				.variants.filter(v => v.name === config.variant)[0];

			// check constructor variables
			getMatchingConstructors(config, variant);

			// add import and extend placeholder replacement
			imports += 'import "' + variant.implementation.path + '";\n';
			extend += ", " + variant.implementation.name;

			// add constructor placeholder replacement
			let constructorVariables = "";
			for (let i=0; i<config.constructorVariable.length; ++i)
				constructorVariables += (typeof config.constructorVariable[i] === "string" ?
					'"' + config.constructorVariable[i] + '"' : config.constructorVariable[i]) + ", ";
			constructor += variant.implementation.name + '(' + constructorVariables.slice(0, -2) + ') ';

			// add test placeholder replacement
			let testData = variant.test.data(config.constructorVariable);
			test += 'require("' + variant.test.path + '").test(Contract, accounts,' + JSON.stringify(testData) + ');';
		});

		// replace placeholder
		contract = contract.replace("$NAME", config.name);
		contract = contract.replace("$COMPILER_VERSION", config.compilerVersion);
		contract = contract.replace("$IMPORTS", imports);
		contract = contract.replace("$EXTENDS", extend);
		contract = contract.replace("$CONSTRUCTOR", constructor.slice(0, -1));
		Test = Test.replace("$TEST", test);
		Test = Test.replace("$NAME", config.name);
		Deploy = Deploy.replace("$NAME", config.name);

		// write contract, tests and deployment configuration
		fs.writeFileSync(this.contract, contract);
		fs.writeFileSync(this.test, Test);
		fs.writeFileSync(this.deploy, Deploy);
	}

	/**
	 * Builds the binaries
	 * @return {out} returns console log of compilation and executionTime
	 */
	build(){
		let start = Date.now();
		this.generate();
		let out = {};
		child_process.execSync("npm install", {cwd: this.path}).toString();
		out.log = child_process.execSync("npm run compile", {cwd: this.path}).toString();
		let end = Date.now();
		out.executionTime = end-start;
		return out;
	}

	/**
	 * Builds source in one file
	 * This can be used to verify a Contract on {@link https://etherscan.io/|Etherscan}
	 * @see {@link https://medium.com/coinmonks/how-to-verify-and-publish-on-etherscan-52cf25312945|How to Verify and Publish on Etherscan}
	 */
	buildOneFileSource(){
		let start = Date.now();
		this.generate();
		let out = {};
		child_process.execSync("npm install", {cwd: this.path}).toString();
		out.log = child_process.execSync("npm run compileOneFile", {cwd: this.path}).toString();
		let end = Date.now();
		out.executionTime = end-start;
		return out;
	}

	/**
	 * Tests the generated Contract
	 * @return {out} returns console log of test and executionTime
	 */
	runTests(){
		let start, end;
		try {
            start = Date.now();
            this.generate();
            let out = {};
            child_process.execSync("npm install", {cwd: this.path}).toString();
            out.log = child_process.execSync("npm test", {cwd: this.path}).toString();
            out.log = out.log.replace(/\u001b\[[0-9]+[a-z]/ug, "");
            end = Date.now();
            out.executionTime = end - start;
            return out;
        } catch (e) {
			end = end || Date.now();
			throw {
				log: (e.stdout && e.stdout.toString()) || e.message,
				executionTime: end - start
			};
        }
	}
}