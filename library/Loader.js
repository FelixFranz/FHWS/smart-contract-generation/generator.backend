const fs = require("fs-extra");

let dictionary = {
    config: __dirname.replace(/\\/g, "/") + "/../config",
    lib: {
        "Loader": "/Loader",
        "Logger": "/Logger",
        "Cron": "/Cron",
        "RequestProcessor": "/RequestProcessor",
	    "SessionManager": "/SessionManager",
        "Session": "/Session",
        "Generator": "/Generator",
    },
    resource: {
        "Contract": "/../resource/Contract.sol",
        "Test": "/../resource/Test.js",
        "Deploy": "/../resource/Deploy.js"
    }
};


exports.init = function(){
    return {
        getLoader: exports.getLoader,
        get: get
    };
};

/**
 * Loads only the loader itself
 * @returns The loader
 */
exports.getLoader = function (){
    return get("lib", "Loader")
};

/**
 * Loads a libraries, config, ..-
 * @returns a loadable Object
 */
function get(){
    switch (arguments[0]){
        case "config":
            return require(dictionary.config);
	    case "lib":
		    if (!dictionary.lib[arguments[1]])
			    throw "Loader: Library " + arguments[1] + " does not exist!";
		    if (!require(__dirname.replace(/\\/g, "/") + dictionary.lib[arguments[1]]).init)
			    throw "Library " + arguments[1] + " doesn't have an init function";
		    return require(__dirname.replace(/\\/g, "/") + dictionary.lib[arguments[1]]).init();
	    case "resource":
		    if (!dictionary.resource[arguments[1]])
			    throw "Loader: Resource " + arguments[1] + " does not exist in the config!";
		    if (!fs.existsSync(__dirname.replace(/\\/g, "/") + dictionary.resource[arguments[1]]))
			    throw "Loader: Resource " + arguments[1] + " does not exist!";
		    return fs.readFileSync(__dirname.replace(/\\/g, "/") + dictionary.resource[arguments[1]]).toString();
        default:
            throw "Loader: Type " + arguments[0] + " does not exist!";
    }
}
