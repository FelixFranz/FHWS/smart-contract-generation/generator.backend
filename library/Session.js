const Loader = require(__dirname + "/Loader.js").getLoader();
const Logger = Loader.get("lib", "Logger");
const Generator = Loader.get("lib", "Generator");
const Config = Loader.get("config").session;
const fs = require('fs-extra');

exports.init = function(){
    return Session;
};

/**
 * updates last used timestamp
 * @param path path to the Session
 */
function updateLastUsed(path){
	if (path === undefined)
		throw new Error("path must not be undefined");
    fs.writeFileSync(path + "lastUsed", Date.now());
}

/**
 * get files for absolute path
 * @param path absolute path
 * @returns [*] all files
 */
function getFiles(path){
    if ("/" === path.slice(-1))
        path = path.slice(0, -1);
    let result = fs.readdirSync(path);
    for (var i=0; i<result.length; ++i){
        result[i]=path + "/" + result[i]
        if (fs.lstatSync(result[i]).isDirectory()){
            result[i] = getFiles(result[i]);
        }
    }
    return [].concat.apply([], result);
}

/**
 * Checks if there is a matching constructor
 * @param config template.js config
 * @param variant variant config file
 * @returns matchingConstrutors
 */
function getMatchingConstructors(templateName, templateVariant, constructorVariable, allTemplates){
	let variant;
	allTemplates.forEach(t =>{
		if (templateName === t.name)
			t.variants.forEach(v => {
				if (templateVariant === v.name)
					variant = v;
			});
	});
	if  (!variant)
		throw new Error("Template variant not found");

	let matchingConstructors = variant.constructor.filter(c => {
		if (c.parameters.length !== constructorVariable.length)
			return false;
		for (let i=0; i<c.parameters.length; ++i){
			if (c.parameters[i].type !== typeof constructorVariable[i])
				return false;
			if (c.parameters[i].higherequal && c.parameters[i].higherequal > constructorVariable[i])
				return false;
			if (c.parameters[i].higher && c.parameters[i].higher >= constructorVariable[i])
				return false;
			if (c.parameters[i].lower && c.parameters[i].lower <= constructorVariable[i])
				return false;
			if (c.parameters[i].lowerequal && c.parameters[i].lowerequal < constructorVariable[i])
				return false;
		}
		return true;
	});
	if (matchingConstructors.length <= 0){
		Logger.log(Logger.levels.WARNING, "There is no matching constructor for " + templateName + ":" + templateVariant + " in the generatorConfig!", "Generator",
			{name: templateName, variant: templateVariant, constructorVariable: constructorVariable});
		throw new Error("There is no matching constructor for " + templateName + ": " + templateVariant + " in the generatorConfig!");
	}
	return matchingConstructors;
}

class Session{
	constructor(id){
		this.id = id;
		this.path = Config.location + id + "/";
	}

    /**
	 * Deletes current session
     */
	delete(){
		fs.removeSync(this.path);
        Object.keys(this).forEach(key => delete this[key]);
        delete this;
	}

    /**
     * Returns last used timestamp
     * @returns {*} milliseconds since epoch
     */
	lastUsed(){
	    try {
		    return fs.readFileSync(this.path + "lastUsed").toString();
	    } catch (e) {
	        return 0;
	    }
	}

    /**
     * Returns the Generator Config
     * @returns {any} generator config object
     */
    getGeneratorConfig(){
        updateLastUsed(this.path);
        return JSON.parse(fs.readFileSync(this.path + "generator.json"));
    }

	/**
	 * Replaces the current generatorConfig
	 * @param generatorConfig new generatorConfig
	 */
	replaceGeneratorConfig(generatorConfig){
        updateLastUsed(this.path);
		fs.writeFileSync(this.path + "generator.json", JSON.stringify(generatorConfig, null, 2));
	}

    /**
	 * Returns all available templates
     * @returns {*} All templates with meta information and variants
     */
	getAllTemplates(){
        updateLastUsed(this.path);
		return require(this.path + "templates")
	}

    /**
	 * Returns templates that are incompatible with current template selection
     * @returns {any[]} incompatible templates
     */
	getIncompatibleTemplates(){
        let templates = this.getAllTemplates();
        let addedTemplates = this.getGeneratorConfig().templates;
        let addedTemplateDescription = templates.filter(t => {
            for (let i=0; i<addedTemplates.length; ++i)
                if (addedTemplates[i].name === t.name)
                    return true;
            return false;
        });
		let incompatibleTemplates = new Set();
        addedTemplateDescription.forEach(d => {
        	if (Array.isArray(d.incompatibleTemplates))
        		d.incompatibleTemplates.forEach(e => incompatibleTemplates.add(e))
        });
		return Array.from(incompatibleTemplates);
    }

    /**
     * Returns all available templates that were not added to the generator config
     * @returns {any[]} templates array with meta information and variants
     */
    getTemplates(){
        updateLastUsed(this.path);
        let templates = this.getAllTemplates();
        let incompatibleTemplates = this.getIncompatibleTemplates();
        let addedTemplates = this.getGeneratorConfig().templates;
        return templates.filter(t => {
        	if (incompatibleTemplates.includes(t.name))
        		return false;
            for (let i=0; i<addedTemplates.length; ++i)
                if (addedTemplates[i].name === t.name)
                    return false;
            return true;
        })
    }

    /**
     * Sets the name of the contract in the generator.json
     * @param name of the contract
     */
    setName(name){
        updateLastUsed(this.path);
        let generatorConfig = this.getGeneratorConfig();
        generatorConfig.name = name;
        this.replaceGeneratorConfig(generatorConfig)
    }

    /**
     * Sets the compilerVersion of the contract in the generator.json
     * @param version of the compiler
     */
    setCompilerVersion(version){
        updateLastUsed(this.path);
        let generatorConfig = this.getGeneratorConfig();
        generatorConfig.compilerVersion = version;
        this.replaceGeneratorConfig(generatorConfig)
    }

    /**
	 * Adds a template to the generatorConfig
     * @param name of the template
     * @param variant of the template to add
     * @param constructorVariable array of constructor variables
     */
	addTemplate(name, variant, constructorVariable){
        updateLastUsed(this.path);
		let generatorConfig = this.getGeneratorConfig();
		generatorConfig.templates.forEach(c => {
			if (c.name === name) {
                Logger.log(Logger.levels.WARNING, "Can not add template " + name + ", because it already exists!", "Session");
                throw new Error("Can not add template " + name + ", because it already exists!");
            }
		});
		getMatchingConstructors(name, variant, constructorVariable, this.getAllTemplates());
		generatorConfig.templates.push({name: name, variant: variant, constructorVariable});
		this.replaceGeneratorConfig(generatorConfig)
	}

    /**
	 * Removes a template from the generatorConfig
     * @param name of the template to remove
     */
	removeTemplate(name){
        updateLastUsed(this.path);
        let generatorConfig = this.getGeneratorConfig();
		generatorConfig.templates = generatorConfig.templates.filter(c => c.name !== name);
        this.replaceGeneratorConfig(generatorConfig)
	}

    /**
     * get all files for directory
     * @param path relative path
     * @returns [*] all containing files
     */
	getFiles(path){
	    let root = this.path;
        if ("/" === root.slice(-1))
            root= root.slice(0, -1);
	    return getFiles(root + path).map(p => {
	        return {path: p, name : p.slice((root + path).length)}
        });
    }

    /**
     * Builds source and returns all files
     * @returns [*] list of all files (name and path)
     */
	getSource(){
	    new Generator(this.id).generate();
        return this.getFiles("/");
    }

    /**
     * Builds binaries and returns all files
     * @returns [*] list of all files (name and path)
     */
    getBinaries(){
	    new Generator(this.id).build();
	    return JSON.parse(fs.readFileSync(this.path + "build/contracts/" + this.getGeneratorConfig().name + ".json").toString());
    }

    /**
     * returns oneFile source file
     * @returns {*} one file source
     */
    getOneFileSource(){
        let generator = new Generator(this.id);
        generator.buildOneFileSource();
        return fs.readFileSync(generator.oneFileSource).toString();
    }

    runTests(){
        return new Generator(this.id).runTests();
    }
}