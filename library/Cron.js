let Loader = require(__dirname + "/Loader.js").getLoader();
const Config = Loader.get("config").cron;
let Logger = Loader.get("lib", "Logger");
let cron = require('cron');

let jobs = [];

exports.init = function(){
	return{
		start: start,
		stop: stop,
		restart: restart,
		register: register,
		time: Config.time
	}
};

/**
 * Function to initialize and start the cron supervisor.
 * Adds cron jobs from the servicePath folder.
 * These jobs should call Cron.register to add a job.
 */
function start() {
	let paths = findJobs(Config.path);
	paths.forEach(p => require(p));

	Logger.log(Logger.levels.INFO, "Added jobs", "Cron");
}

/**
 * Recursive helper function to get all files of the servicePath folder.
 * @param path
 * @returns {*[]} config files
 */
function findJobs(path){
	let fs = require("fs");
	let result = fs.readdirSync(path);
	for (var i=0; i<result.length; ++i){
		result[i]=path + "/" + result[i]
		if (fs.lstatSync(result[i]).isDirectory()){
			result[i] = findJobs(result[i]);
		}
	}
	return [].concat.apply([], result);;
}

/**
 * Function to stop the cron supervisor with all jobs.
 */
function stop() {
	while (jobs.length>0){
		let j = jobs.pop();
		j.job.stop();
		if (j.job.running){
			Logger.log(Logger.levels.INFO, "Can not stop job " + j.name + "!", "Cron");
		} else {
			Logger.log(Logger.levels.WARNING, "Stopped job " + j.name + "!", "Cron");
		}
	}
}

/**
 * Function to restart the cron supervisor.
 */
function restart() {
	stop();
	start();
}

/**
 * This function should be called by a job that provides all neccessary information
 * @param config file that defines the time and the job function
 */
function register(config){
	config.job = new cron.CronJob({
		cronTime: config.time,
		onTick: () => runJob(config.name, config.handle)
	});
	config.job.start();
	if (config.job.running){
		Logger.log(Logger.levels.DEBUG, "Scheduled cron job " + config.name + "!", "Cron");
		jobs.push(config);
	} else {
		Logger.log(Logger.levels.WARNING, "Can not start cron job " + config.name + "!", "Cron");
	}
}

/**
 * This function will be called by cron and calls the handle function
 * @param handle function to be called
 * @returns {Promise<void>} nothing
 */
async function runJob(name, handle){
	let startTimeMs = Date.now();
	let startTimestamp = new Date(startTimeMs);
	startTimestamp.setMilliseconds(0);
	Logger.log(Logger.levels.DEBUG, "Job " + name + " started as scheduled on " + startTimestamp.toLocaleString() + "!", "Cron");
	try {
		let info= {
			name: name,
			startTimestamp: startTimestamp.toLocaleString(),
			startTimeMs: startTimeMs
		}
		await handle(info);
	} catch (e) {
		Logger.log(Logger.levels.WARNING, "Job " + name + " failed!", "Cron", e);
	}
}
