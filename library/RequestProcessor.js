const Loader = require(__dirname + "/Loader.js").getLoader();
const Logger = Loader.get("lib", "Logger");
const Config = Loader.get("config").api;
const Path = require('path');

let app;
let server;
let zip;

exports.init = function(){
	return{
		start: start,
		stop: stop,
		restart: restart,
		process: process
	}
}

/**
 * Function to initialize and start the api endpoint server.
 */
function start() {
	app = require('express')();
	zip = require('express-zip');
	let bodyParser = require('body-parser');
	app.use(bodyParser.json());

	addEndpoints();

	server = app.listen(Config.port,
		()=> Logger.log(Logger.levels.INFO, "Started listening on http://127.0.0.1:" + Config.port, "RequestProcessor"));
}

/**
 * Adds Endpoints by calling all files (services) in the servicePath folder.
 * These services should call RequestProcessor.process to add themself to the api.
 */
function addEndpoints(){
	let paths = findServices(Config.servicePath);
	paths.forEach(p => require(p));

	Logger.log(Logger.levels.INFO, "Added endpoints", "RequestProcessor");
}

/**
 * Recursive helper function to get all files of the servicePath folder.
 * @param path
 * @returns {*[]}
 */
function findServices(path){
	let fs = require("fs");
	let result = fs.readdirSync(path);
	for (var i=0; i<result.length; ++i){
		result[i]=path + "/" + result[i]
		if (fs.lstatSync(result[i]).isDirectory()){
			result[i] = findServices(result[i]);
		}
	}
	return [].concat.apply([], result);
}

/**
 * Function to stop the api endpoint server.
 */
function stop() {
	server.close(() => Logger.log(Logger.levels.INFO, "Stopped server", "RequestProcessor"));
}

/**
 * Function to restart the api endpoint server.
 */
function restart() {
	stop();
	start();
}

/**
 * This function should be called by a service and provides all neccessary configuration information
 * @param config file that defines request methods, handler functions and validation functions
 */
function process(config){
	let methods = {
		"GET": config.get,
		"POST": config.post,
		"PUT": config.put,
		"DELETE": config.delete
	};

	for (var method in methods){
		if (methods[method]) {
			let handle;
			let validate;
			let link;

			if (typeof methods[method] == "function"){
				handle = methods[method];
				validate = config.validate;
				link = config.link;
			}
			else if (methods[method].handle) {
				handle = methods[method].handle;
				validate = methods[method].validate || config.validate;
				link = methods[method].link || config.link;
			}

			if(!Array.isArray(link)) link = [link];

			let process = (req, res) => handleRequest(req, res, config.path, handle, validate, link);

			switch (method){
				case "GET":
					app.get(config.path, process);
					break;
				case "POST":
					app.post(config.path, process);
					break;
				case "PUT":
					app.put(config.path, process);
					break;
				case "DELETE":
					app.delete(config.path, process);
					break;
			}
		}
	}

	app.all(config.path, (req, res) => {
		res.header("Content-Type", "application/json'");
		res.status(405);
		res.send(JSON.stringify({error: "Wrong request Method!"}));
	});
}

/**
 * Every api call will be redirected to this method
 * This method validates and handles the calls by calling service functions.
 *
 * @param expressReq express request object
 * @param expressRes express response object
 * @param handle handle function (service function)
 * @param validate validate function (service function)
 */
async function handleRequest(expressReq, expressRes, endpoint, handle, validate, link) {
	expressRes.header("Content-Type", "application/json");

	let location = expressReq.protocol + "://" + expressReq.get("host") + expressReq.originalUrl;
	let req = {
		method: expressReq.method,
		query: expressReq.query,
		path: expressReq.params,
		body: expressReq.body,
		endpoint: expressReq.originalUrl,
		location: location.slice(-1) === "/" ? location.slice(0, -1) : location,
		locationRoot: location.replace(new RegExp(endpoint.replace(/:[^\/]*/g, "[^\\/]*")), "").replace(/\/+$/, "")
	};
	if (req.locationRoot.slice(-1) === "/")
		req.locationRoot = req.locationRoot.slice(0, -1);

	try {
		if (validate && false == await validate(req)) {
			expressRes.status(400);
			expressRes.send({error: "Validation failed for " + endpoint + "!"});
			Logger.log(Logger.levels.DEBUG, "Validation failed for " + endpoint + "!", "RequestProcessor");
			return;
		}
	} catch (e) {
		expressRes.status(e.status || 400);
		expressRes.send({error: "Validation failed for " + endpoint + " !", details: e.message || e});
		Logger.log(Logger.levels.DEBUG, "Validation failed for " + endpoint + " !", "RequestProcessor", e);
		return;
	}

	let resObj = function (){
		this.body = undefined;
		this.status = undefined;
		this.header = undefined;
		this.cookie = undefined;
		this.file = undefined;
		this.zip = undefined;
	};
	let res = new resObj();

	try {
		res.body = await handle(req, res) || res.body;
		res.status = res.status || (!res.body && !res.file && !res.zip ? 204: undefined) || ("POST" == req.method ? 201 : 200);
		expressRes.status(res.status);
		if (link !== undefined){
			link = link.map(l => {
                let pathParamRegex = /^.*\/(:[^\/]*).*$/;
                let pathParamPlaceHolder = pathParamRegex.exec(l.url);
                while (pathParamPlaceHolder !== null){
                	pathParamPlaceHolder = pathParamPlaceHolder[1];
                    l.url = l.url.replace(pathParamPlaceHolder, req.path[pathParamPlaceHolder.slice(1)]);
                    pathParamPlaceHolder = pathParamRegex.exec(l.url);
                }
                let responseRegex = /^.*\/(\$[^\/]*).*$/;
                let responsePlaceHolder = responseRegex.exec(l.url);
                while (responsePlaceHolder !== null){
                	responsePlaceHolder = responsePlaceHolder[1];
                    l.url = l.url.replace(responsePlaceHolder, res.body[responsePlaceHolder.slice(8)]);
                    responsePlaceHolder = responseRegex.exec(l.url);
                }
				return l;
			});
			link = link.map(l => "<" + req.locationRoot + l.url + '>; title="' + l.name + '"');
			let completeLink = "";
			link.forEach(l => completeLink += l + ", ");
			completeLink = completeLink.slice(0, -2);
			expressRes.set("Link", completeLink);
		}
		if (typeof res.header === "object")
			Object.keys(res.header).forEach(key => expressRes.set(key, res.header[key]));
		if (typeof res.cookie === "object")
			Object.keys(res.cookie).forEach(key =>
				expressRes.cookie(key, res.cookie[key].value, {
					maxAge: res.cookie[key].maxAge,
					domain: res.cookie[key].domain
				})
			);
		if (!!res.file) {
			expressRes.removeHeader("content-type");
			expressRes.sendFile(Path.resolve(res.file));
		} else if (!!res.zip){
            expressRes.removeHeader("content-type");
            expressRes.zip(res.zip.content, res.zip.name + ".zip");
        } else
			expressRes.send(expressRes.getHeader("content-type").includes("application/json") ? JSON.stringify(res.body) : res.body);
		Logger.log(Logger.levels.DEBUG, "Handled " + req.method + " request for " + endpoint + "!", "RequestProcessor");
	} catch (e) {
		Logger.log(Logger.levels.WARNING, "Could not handle request for " + endpoint + "!", "RequestProcessor", {status: e.status, message: e.message, stack: e.stack});
		expressRes.status(e.status || 500);
		expressRes.send(JSON.stringify({error: "Could not handle request!", details: e.message || e}))
	}

}