let Loader = require(__dirname + "/Loader.js").getLoader();
let Config = Loader.get("config").log;
const fs = require('fs');

/**
 * all possible logging levels
 * @type {{ERROR, WARNING, INFO, DEBUG, TRACE}}
 */
let levels = {
    ERROR: 0,
    WARNING: 1,
    INFO: 2,
    DEBUG: 3,
    TRACE: 4
};

exports.init = function(){
    return{
        log: log,
        levels: levels
    }
};

/**
 * Creates a log entry
 * @param level Logging level provided by Logger.level
 * @param message Logging message
 * @param source Source of log entry
 * @param details (optional) Additional details
 */
function log(level, message, source, details){
    if (level <= this.levels[Config.level.toUpperCase()]) {
        let out = parseMessage(level, message, source, details);
        console.log(out);
        writeToFile(out);
    }
}

/**
 * Helper function to parse the logging message
 * @param level Logging level provided by Logger.level
 * @param message Logging message
 * @param source Source of log entry
 * @param details (optional) Additional details
 * @returns parsed logging message
 */
function parseMessage(level, message, source, details){
    if (details instanceof Error)
        details = {
            message: details.message,
            stack: details.stack
        };
    return ""
        + new Date().toLocaleString()
        + " [" + Object.keys(levels)[level] + "]: "
        + ( source ? source + ": " : "" )
        + message
        + ( details ? "\n" + JSON.stringify(details) : "" );
}

/**
 * Writes log entry to file
 * @param out log entry
 */
function writeToFile(out){
    let logFolder = Config.folder;
    if (!fs.existsSync(logFolder)){
        fs.mkdirSync(logFolder);
    }
    removeLogs();
    fs.appendFileSync(logFolder + "backend_1.log", out + "\n");
}

/**
 * removes old log files if they are too big
 * it keeps number of log files as provided in the config
 */
function removeLogs(){
    let logFolder = Config.folder;
    if(fs.existsSync(logFolder + "backend_1.log") &&
        fs.statSync(logFolder + "backend_1.log").size > Config.fileSize){
        if (fs.existsSync(logFolder + "backend_" + Config.fileNumber -1 + ".log"))
            fs.unlinkSync(logFolder + "backend_" + Config.fileNumber -1 + ".log");
        for (let i=Config.fileNumber -1; i>0 ; --i){
            if (fs.existsSync(logFolder + "backend_" + i + ".log"))
                fs.renameSync(logFolder + "backend_" + i + ".log", logFolder + "backend_" + (i +1) + ".log");
        }
    }
}
