const Loader = require(__dirname + "/Loader.js").getLoader();
const Session = Loader.get("lib", "Session");
const Config = Loader.get("config").session;
const Logger = Loader.get("lib", "Logger");
const fs = require('fs-extra');

exports.init = function(){
	if (!fs.existsSync(Config.location)){
		fs.mkdirSync(Config.location);
	}
    return new SessionManager();
};

/**
 * Generates unique sessionId
 * @returns {string} sessionId
 */
function generateUniqueId(){
    let id;
    do
	    id = (Math.random().toString(36).substr(2) + Math.random().toString(36).substr(2)).substr(0, 20);
    while(fs.existsSync(Config.location + id));
    return id;
}

class SessionManager{

	/***
	 * Creates a new session
	 * @returns {*} new session
	 */
    create(){
	    let id = generateUniqueId();

	    fs.mkdirSync(Config.location +  id);
	    fs.copySync(Config.contractLibrary, Config.location + id);
        fs.removeSync(Config.location + id + "/.git");
        fs.removeSync(Config.location + id + "/.gitlab-ci.yml");
	    return this.get(id);
    }

	/**
	 * Returns a session
	 * @param id of the session to be returned
	 * @returns {*} session
	 */
	get(id){
		if (!fs.existsSync(Config.location + id)){
			Logger.log(Logger.levels.WARNING, "Provided session id does not exist!", "SessionManager");
			throw new Error("Provided session id does not exist");
		}
		return new Session(id);
    }

	/**
	 * Imports an existion session configuration
	 * @param generatorConfig object that contains session configuration
	 * @returns {*} session
	 */
	import(generatorConfig){
		let session = this.create();
		session.replaceGeneratorConfig(generatorConfig);
		return session;
    }

	/**
	 * Return ids of all available sessions
	 * @return [*] session id
	 */
	getAllIds(){
		return fs.readdirSync(Config.location);
    }
}