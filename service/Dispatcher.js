const Loader = require(__dirname + "/../library/Loader.js").getLoader();
const RequestProcessor = Loader.get("lib", "RequestProcessor");

function handleGet(req, res){
	res.header= {
		"content-type": "text/html"
	};
	return "<html><head><title>Smart Contract Generator API</title></head><body><h1>Smart Contract Generator API</h1><p>This is the api of the Smart Contract Generator!<br />Visit the <a href='https://gitlab.com/FelixFranz/FHWS/smart-contract-generation'>Gitlab Repository</a> for more information or just use the <a href='/'>frontend</a>!</p></body></html>";
}

RequestProcessor.process({
	path: "/",
	get: handleGet,
	link: {
		name: "Create/Import Session",
		url: "/session"
	}
});
