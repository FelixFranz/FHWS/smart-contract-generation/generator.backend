const Loader = require(__dirname + "/../../library/Loader.js").getLoader();
const RequestProcessor = Loader.get("lib", "RequestProcessor");
const SessionManager = Loader.get("lib", "SessionManager");

function handleGet(req, res){
    try {
	    let result = SessionManager.get(req.path.sessionId).runTests();
	    return result;
    } catch (e) {
        res.status = 500;
        return e;
    }
}

RequestProcessor.process({
    path: "/session/:sessionId/test",
    get: handleGet,
    link: {
        name: "Manage Session",
        url: "/session/:sessionId"
    }
});
