const Loader = require(__dirname + "/../../library/Loader.js").getLoader();
const RequestProcessor = Loader.get("lib", "RequestProcessor");
const SessionManager = Loader.get("lib", "SessionManager");

function handleGet(req, res){
	return SessionManager.get(req.path.sessionId).getTemplates()
}

RequestProcessor.process({
	path: "/session/:sessionId/availabletemplates",
	get: handleGet,
    link: {
        name: "Manage Session",
        url: "/session/:sessionId"
    }
});
