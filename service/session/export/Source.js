const Loader = require(__dirname + "/../../../library/Loader.js").getLoader();
const RequestProcessor = Loader.get("lib", "RequestProcessor");
const SessionManager = Loader.get("lib", "SessionManager");

function handleGet(req, res){
	let Session = SessionManager.get(req.path.sessionId);
	res.header = {
		"x-file-name": "Source.zip"
	};
    res.zip = {
        name: "Source",
        content: Session.getSource()
    };
}

RequestProcessor.process({
	path: "/session/:sessionId/export/source",
	get: handleGet,
    link: {
        name: "Manage Session",
        url: "/session/:sessionId"
    }
});
