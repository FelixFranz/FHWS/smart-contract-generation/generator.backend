const Loader = require(__dirname + "/../../../library/Loader.js").getLoader();
const RequestProcessor = Loader.get("lib", "RequestProcessor");
const SessionManager = Loader.get("lib", "SessionManager");

function handleGet(req, res){
    res.header = {
        "content-type": "text/plain",
	    "x-file-name": "OneFileSource.sol"
    };
    return SessionManager.get(req.path.sessionId).getOneFileSource();
}

RequestProcessor.process({
    path: "/session/:sessionId/export/onefilesource",
    get: handleGet,
    link: {
        name: "Manage Session",
        url: "/session/:sessionId"
    }
});
