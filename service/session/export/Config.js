const Loader = require(__dirname + "/../../../library/Loader.js").getLoader();
const RequestProcessor = Loader.get("lib", "RequestProcessor");
const SessionManager = Loader.get("lib", "SessionManager");

function handleGet(req, res){
    let Session = SessionManager.get(req.path.sessionId);
	res.header = {
		"x-file-name": "Config.json"
	};
    return Session.getGeneratorConfig()
}

RequestProcessor.process({
    path: "/session/:sessionId/export/config",
    get: handleGet,
    link: {
        name: "Manage Session",
        url: "/session/:sessionId"
    }
});
