const Loader = require(__dirname + "/../../library/Loader.js").getLoader();
const RequestProcessor = Loader.get("lib", "RequestProcessor");
const SessionManager = Loader.get("lib", "SessionManager");

function handleGet(req, res) {
	return SessionManager.get(req.path.sessionId).getGeneratorConfig().templates;
}

function validatePost(req){
	if (!req.body.name || !req.body.variant || !req.body.constructorVariable)
		throw "Name, variant and constructorVariable are required in the request body."
}

function handlePost(req, res){
	try {
		SessionManager.get(req.path.sessionId).addTemplate(req.body.name, req.body.variant, req.body.constructorVariable);
	} catch (e) {
		if (e.message.includes("There is no matching constructor for")){
			res.status = 400;
			return e.message;
		}
		else throw e;
	}
}

function validateDelete(req){
    if (!req.body.name)
        throw "Name is required in the request body."
}

function handleDelete(req, res){
    SessionManager.get(req.path.sessionId).removeTemplate(req.body.name);
}

RequestProcessor.process({
	path: "/session/:sessionId/template",
	get: handleGet,
	post:{
		validate: validatePost,
		handle: handlePost
    },
	delete:{
		validate: validateDelete,
		handle: handleDelete
    },
    link: {
        name: "Manage Session",
        url: "/session/:sessionId"
    }
});
