const Loader = require(__dirname + "/../../library/Loader.js").getLoader();
const RequestProcessor = Loader.get("lib", "RequestProcessor");
const SessionManager = Loader.get("lib", "SessionManager");

function handlePost(req, res){
	let Session ;
	if (Object.keys(req.body).length > 0)
		Session = SessionManager.import(req.body);
	else
		Session = SessionManager.create();
	return {
		id: Session.id,
		url: req.location + "/" + Session.id
	};
}

RequestProcessor.process({
	path: "/session",
	post: handlePost,
	link: {
		name: "Manage Session",
		url: "/session/$result.id"
	}
});
