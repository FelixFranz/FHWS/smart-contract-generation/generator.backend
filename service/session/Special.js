const Loader = require(__dirname + "/../../library/Loader.js").getLoader();
const RequestProcessor = Loader.get("lib", "RequestProcessor");
const SessionManager = Loader.get("lib", "SessionManager");

function handleGet(req, res){
	return SessionManager.get(req.path.sessionId).getGeneratorConfig()
}

function handlePut(req, res){
	let Session = SessionManager.get(req.path.sessionId);
	if (!!req.body.name) Session.setName(req.body.name);
	if (!!req.body.compilerVersion) Session.setCompilerVersion(req.body.compilerVersion);
}

function handleDelete(req, res){
	SessionManager.get(req.path.sessionId).delete();
}

RequestProcessor.process({
	path: "/session/:sessionId",
	get: {
		handle: handleGet,
		link: [
            {
                name: "Available templates",
                url: "/session/:sessionId/availabletemplates"
            }, {
                name: "Add/Remove/Show current templates",
                url: "/session/:sessionId/template"
            }, {
                name: "Test generated Smart Contract",
                url: "/session/:sessionId/test"
            }, {
                name: "Export generation config",
                url: "/session/:sessionId/export/config"
            }, {
                name: "Export Source files",
                url: "/session/:sessionId/export/source"
            }, {
                name: "Export binaries",
                url: "/session/:sessionId/export/binaries"
            }, {
                name: "Export one file source",
                url: "/session/:sessionId/export/onefilesource"
            }
        ]
    },
	put: handlePut,
	delete: handleDelete,
    link: {
        name: "Manage Session",
        url: "/session/:sessionId"
    }
});
