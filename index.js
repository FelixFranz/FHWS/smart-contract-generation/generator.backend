const Loader = require(__dirname + "/library/Loader").getLoader();
const Logger = Loader.get("lib", "Logger");
const RequestProcessor = Loader.get("lib", "RequestProcessor");
let Cron = Loader.get("lib", "Cron");

Cron.start();
RequestProcessor.start();
Logger.log(Logger.levels.INFO, "Started backend!", "Starter");