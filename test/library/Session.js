const Loader = require(__dirname + "/../../library/Loader.js").getLoader();
const SessionManager = Loader.get("lib", "SessionManager");
const assert = require('assert');
const fs = require('fs-extra');

describe("Session", () => {

	let Session;
	beforeEach(() => Session = SessionManager.create());

	let sourceFiles = [ 'contracts/Contract.sol',
        'contracts/EIP/EIP165/EIP165_Default.sol',
        'contracts/EIP/EIP165/EIP165_Interface.sol',
        'contracts/EIP/EIP20/EIP20_ApproveByAddAllowance.sol',
        'contracts/EIP/EIP20/EIP20_ApproveBySendingCurrentValue.sol',
        'contracts/EIP/EIP20/EIP20_Default.sol',
        'contracts/EIP/EIP20/EIP20_Interface.sol',
        'contracts/EIP/EIP20/SafeMath.sol',
        'contracts/Migrations.sol',
        'generator.json',
        'lastUsed',
        'migrations/1_initial_migration.js',
        'migrations/2_deploy_contracts.js',
        'package-lock.json',
        'package.json',
        'README.md',
        'templates.js',
        'test/EIP/EIP165/EIP165_Default.js',
        'test/EIP/EIP20/EIP20_ApproveByAddAllowance.js',
        'test/EIP/EIP20/EIP20_ApproveBySendingCurrentValue.js',
        'test/EIP/EIP20/EIP20_Default.js',
        'test/EIP/EIP20/General.js',
        'test/Test.js',
        'truffle.js' ];
	let buildFiles = [
        'contracts/EIP165_Default.json',
        'contracts/EIP165_Interface.json',
        'contracts/EIP20_ApproveByAddAllowance.json',
        'contracts/EIP20_ApproveBySendingCurrentValue.json',
        'contracts/EIP20_Default.json',
        'contracts/EIP20_Interface.json',
        'contracts/Migrations.json',
        'contracts/MyContract.json',
        'contracts/SafeMath.json'
    ];

	describe("delete", () => {
	    it("should delete all Session files", () => {
	        let path = Session.path;
	        Session.delete();
            assert.ok(!fs.existsSync(path), "Session folder must not exist after deleting");
        });
	    it("should delete the Session object", () => {
	        Session.delete();
	        assert.throws(() => Session.setName("test"), "It should throw an exception");
	        assert.deepEqual(Session, {}, "It contain an empty object");
        });
    });

	describe("lastUsed", () => {
	    it("should display last used timestamp", async () => {
	        let before = Date.now();
	        Session.setName("Test");
	        let after = Date.now();
            let provided = Session.lastUsed();
            assert.ok(before < provided < after, "Provided timestamp should be correct.");
        });
    });

    describe("getGeneratorConfig", () => {
        it("should return the generatorConfig", () => {
            let generatorConfig = Session.getGeneratorConfig();
            assert.ok(typeof generatorConfig, "object", "generatorConfig should be an object");
            assert.ok(typeof generatorConfig.name, "string", "generatorConfig.name should be a string");
            assert.ok(Array.isArray(generatorConfig.templates), "generatorConfig.templates should contain an array of templates");

            let expectedGeneratorConfig = JSON.parse(fs.readFileSync(Session.path + "generator.json"));
            assert.equal(JSON.stringify(generatorConfig), JSON.stringify(expectedGeneratorConfig), "The generator.json should use the right file");
        });
    });

    describe("replaceGeneratorConfig", () => {
        it("should replace the generator config", () => {
            let expectedGeneratorConfig = {name: "Test", templates: []};
            Session.replaceGeneratorConfig(expectedGeneratorConfig);
            let generatorConfig = JSON.stringify(Session.getGeneratorConfig());
            expectedGeneratorConfig = JSON.stringify(expectedGeneratorConfig);
            assert.equal(generatorConfig, expectedGeneratorConfig, "The generator.json should contain the new config");
        });
    });

	describe("getAllTemplates", () => {
		it("should return all templates", () => {
			let templates = Session.getAllTemplates();
			assert.ok(Array.isArray(templates), "Template should contain an array of templates");
			assert.ok(typeof templates[0], "object", "Template[0] should be an object");
			assert.ok(typeof templates[0].name, "string", "Template[0].name should be a string");
			assert.ok(typeof templates[0].description, "string", "Template[0].description should be a string");
			assert.ok(typeof templates[0].url, "string", "Template[0].url should be a string");
			assert.ok(typeof templates[0].variants, "object", "Template[0].variants should be a object");
		});
	});

	describe("getIncompatibleTemplates", ()=>{
        it("should return incompatible templates", () => {
            Session.addTemplate("EIP-173: ERC-173 Contract Ownership Standard", "Default", []);
            let incompatibleTemplates = Session.getIncompatibleTemplates();
            assert(incompatibleTemplates.length >= 1, "There need to be at least one incompatible template");
            assert.equal(1, incompatibleTemplates.filter(t=> t === "OWN-1: Mortability Standard").length,
                "OWN-1: Mortability Standard need to be an incompatible template")
        });
    });

    describe("getTemplates", () => {

        it("should return all templates", () => {
            let templates = Session.getTemplates();
            assert.ok(Array.isArray(templates), "Template should contain an array of templates");
            assert.ok(typeof templates[0], "object", "Template[0] should be an object");
            assert.ok(typeof templates[0].name, "string", "Template[0].name should be a string");
            assert.ok(typeof templates[0].description, "string", "Template[0].description should be a string");
            assert.ok(typeof templates[0].url, "string", "Template[0].url should be a string");
            assert.ok(typeof templates[0].variants, "object", "Template[0].variants should be a object");
        });

        it('should not return "EIP-20: ERC-20 Token Standard"', () => {
            Session.addTemplate("EIP-20: ERC-20 Token Standard", "Default", ["TestToken", "Test", 42, 1000]);
            let templates = Session.getTemplates();
            templates.forEach(t => {
                assert.notEqual(t.name, "EIP-20: ERC-20 Token Standard", '"EIP-20: ERC-20 Token Standard" should not be available any more');
            });
        });

        it('should return "EIP-20: ERC-20 Token Standard"', () => {
            Session.addTemplate("EIP-20: ERC-20 Token Standard", "Default", ["TestToken", "Test", 42, 1000]);
            Session.removeTemplate("EIP-20: ERC-20 Token Standard");
            let templates = Session.getTemplates();
            let num = templates.filter((t) => t.name === "EIP-20: ERC-20 Token Standard").length;
            assert.equal(num, 1, '"EIP-20: ERC-20 Token Standard" does not exist');
        });

        it("should not contain OWN-1: Mortability Standard", () => {
            Session.addTemplate("EIP-173: ERC-173 Contract Ownership Standard", "Default", []);
            let templates = Session.getTemplates().filter(t => t.name === "OWN-1: Mortability Standard");
            assert.equal(0, templates.length, "OWN-1: Mortability Standard must not exist")
        });
    });

    describe("setName", () => {
        it("should replace the name in the generator config", () => {
            let expectedName = "HiIAmASmartContract";
            Session.setName(expectedName);
            assert.equal(Session.getGeneratorConfig().name, expectedName, "The generator.json should contain the new name");
        });
    });

    describe("addTemplate", () => {
        it("should add a template", () => {
            let expectedTemplate = {name:"EIP-20: ERC-20 Token Standard", variant: "Default", constructorVariable: ["TestToken", "Test", 42, 1000]};
            Session.addTemplate(expectedTemplate.name, expectedTemplate.variant, expectedTemplate.constructorVariable);
            let addedTemplates = Session.getGeneratorConfig().templates;
            assert.equal(addedTemplates.length, 1, "There should only be one template");
            assert.equal(typeof addedTemplates[0], "object", "template[0] should be an object");
            assert.deepEqual(addedTemplates[0], expectedTemplate, "The template object must contain eqal values");
        });
        it("should not add a template that already exists", () => {
            let expectedTemplate = {name:"EIP-20: ERC-20 Token Standard", variant: "Default", constructorVariable: ["TestToken", "Test", 42, 1000]};
            Session.addTemplate(expectedTemplate.name, expectedTemplate.variant, expectedTemplate.constructorVariable);
            assert.throws( () =>
                Session.addTemplate(expectedTemplate.name, expectedTemplate.variant, expectedTemplate.constructorVariable),
            "It should throw an exception if a template that should be added already exists");
        });
        it("should fail with no matching constructor (wrong amount of variables)", () => {
            Session.removeTemplate("EIP-20: ERC-20 Token Standard");
            assert.throws(() => Session.addTemplate("EIP-20: ERC-20 Token Standard", ",", ["fail"]), "If there is no matching constructor in the generatorConfig an error need to be thrown!");
        });
        it("should fail with no matching constructor (wrong variable type)", () => {
            Session.removeTemplate("EIP-20: ERC-20 Token Standard");
            assert.throws(() => Session.addTemplate("EIP-20: ERC-20 Token Standard", "Default", ["TestToken", 2, 42, 1000]), "If there is no matching constructor in the generatorConfig an error need to be thrown!");
        });

        it("should fail with no matching constructor (wrong variable condition, 3rd variable to small)", () => {
            Session.removeTemplate("EIP-20: ERC-20 Token Standard");
            assert.throws(() => Session.addTemplate("EIP-20: ERC-20 Token Standard", "Default", ["TestToken", "X", 0, 1000]), "If there is no matching constructor in the generatorConfig an error need to be thrown!");
        });
    });

    describe("removeTemplate", () => {
        it("should remove a template", () => {
            let expectedTemplate = {name:"EIP-20: ERC-20 Token Standard", variant: "Default", constructorVariable: ["TestToken", "Test", 42, 1000]};
            Session.addTemplate(expectedTemplate.name, expectedTemplate.variant, expectedTemplate.constructorVariable);
            Session.removeTemplate(expectedTemplate.name);
            let num = Session.getGeneratorConfig().templates.filter((t) => t.name === "EIP-20: ERC-20 Token Standard").length;
            assert.equal(num, 0, '"EIP-20: ERC-20 Token Standard" exists');
        });
        it("should do nothing if the template that should be removed does not exist", () => {
            let expectedTemplate = {name:"EIP-20: ERC-20 Token Standard", variant: "Default", constructorVariable: ["TestToken", "Test", 42, 1000]};
            Session.addTemplate(expectedTemplate.name, expectedTemplate.variant, expectedTemplate.constructorVariable);
            Session.removeTemplate(expectedTemplate.name);
        });
    });

    describe("getSource", () => {
        it("Should return zip config for source", () => {
            Session.addTemplate("EIP-20: ERC-20 Token Standard", "Default", ["TestToken", "Test", 42, 1000]);
            let zipConfig = Session.getSource();
            sourceFiles.forEach(f => assert(zipConfig.map(c => c.name).includes(f)));
        })
    });

    describe("getBinaries", function (){
        this.timeout(60000);
        it("Should return binaries as a json", () => {
            Session.addTemplate("EIP-20: ERC-20 Token Standard", "Default", ["TestToken", "Test", 42, 1000]);
            let binaries = Session.getBinaries();
            assert(binaries !== undefined, "Binaries must not be undefined");
            assert(typeof binaries.contractName == "string", "contractName must exist and be a string inside the binaries")
        })
    });

    describe("getOneFileSource", function (){
        this.timeout(50000);
        it("Should return one file source", () => {
            Session.addTemplate("EIP-20: ERC-20 Token Standard", "Default", ["TestToken", "Test", 42, 1000]);
            let oneFileSource = Session.getOneFileSource();
            assert(oneFileSource.includes("contract MyContract is EIP165_Default, EIP20_Default"), "One file source must include generated contract")
        })
    });

    describe("runTests", function (){
        this.timeout(60000);
        it("should succeed", () => {
            Session.addTemplate("EIP-20: ERC-20 Token Standard", "Default", ["TestToken", "Test", 42, 1000]);
            let result = Session.runTests();
            assert.ok(typeof result === "object", "Result needs to be an object");
            assert.ok(typeof result.log === "string", "Result log output needs to be a string");
            assert.ok(typeof result.executionTime === "number", "Result executionTime output needs to be a number");
        });
    });

});