const Loader = require(__dirname + "/../../library/Loader.js").getLoader();
const SessionManager = Loader.get("lib", "SessionManager");
const assert = require('assert');
const fs = require('fs-extra');

describe("SessionManager", () => {

	describe("create", () => {
		it("should create new session", () => {
			let Session = SessionManager.create();
			assert.equal(typeof Session, "object", "Session should be an object");
			assert.equal(typeof Session.id, "string", "Session should contain a id");
			assert.equal(typeof Session.path, "string", "Session should contain a path");
		});
	});

	describe("get", () => {
		it("should open return an existing session", () => {
			let oldSession = SessionManager.create();
			let Session = SessionManager.get(oldSession.id);
			assert.equal(typeof Session, "object", "Session should be an object");
			assert.equal(typeof Session.id, "string", "Session should contain a id");
			assert.equal(typeof Session.path, "string", "Session should contain a path");
		});
		it("should not be able to open a session if the id is wrong", () => {
			assert.throws(()=>{
				SessionManager.get("fail");
			}, Error, "Should fail if id does not exist")
		});
	});

	describe("import", () => {
		it("should add an imported config to session", () => {
			let expectedGeneratorConfig = {name: "Test", templates: []};
			let Session = SessionManager.import(expectedGeneratorConfig);
			let generatorConfig = fs.readFileSync(Session.path + "generator.json").toString();
			expectedGeneratorConfig = JSON.stringify(expectedGeneratorConfig, null, 2);
			assert.equal(generatorConfig, expectedGeneratorConfig, "The generator.json should contain the new config");
		});
	});
});