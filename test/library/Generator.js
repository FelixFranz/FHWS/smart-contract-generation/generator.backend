const Loader = require(__dirname + "/../../library/Loader.js").getLoader();
const SessionManager = Loader.get("lib", "SessionManager");
const GeneratorLib = Loader.get("lib", "Generator");
const assert = require('assert');
const fs = require('fs-extra');

describe("Generator", () => {

	let Session;
	let Generator;
	let expectedContract = '' +
	'pragma solidity ^0.4.23;\n' +
	'\n' +
	'import "./EIP/EIP165/EIP165_Default.sol";\n' +
	'import "./EIP/EIP20/EIP20_Default.sol";\n' +
	'\n' +
	'contract Test is EIP165_Default, EIP20_Default {\n' +
	'\n' +
	'    constructor() EIP20_Default("TestToken", "Test", 42, 1000) public {\n' +
	'        // Your own Smart Contract 😎\n' +
	'    }\n' +
	'}';
	let expectedTest = '' +
		'let Contract = artifacts.require("./Test.sol");\n' +
		'\n' +
		'contract(Contract, (accounts) => {\n' +
		'\trequire("./EIP/EIP165/EIP165_Default").test(Contract, accounts);\n' +
		'\trequire("./EIP/EIP20/EIP20_Default.js").test(Contract, accounts,{"name":"TestToken","symbol":"Test","decimals":42,"totalSupply":1000});\n' +
		'});';
	let expectedDeployment = '' +
		'let Contract = artifacts.require("./Test.sol");\n' +
		'\n' +
		'module.exports = function(deployer) {\n' +
		'\tdeployer.deploy(Contract);\n' +
		'};';
	let failTest = '' +
		'module.exports.test = function (contract, accounts){\n' +
		'    describe("EIP-165_Default", () => {\n' +
		'        it("test should fail", () => {\n' +
		'            assert.fail();\n' +
		'        });\n' +
		'    });\n' +
		'};';
	beforeEach(() => {
		Session = SessionManager.create();
		Session.setName("Test");
		Session.addTemplate("EIP-20: ERC-20 Token Standard", "Default", ["TestToken", "Test", 42, 1000]);
		Generator = new GeneratorLib(Session.id);
	});

	describe("clean", function(){
		this.timeout(100000);
		it("should clean binaries", () => {
			Generator.build();
			Generator.buildOneFileSource();
			Generator.clean();
			assert.ok(!fs.existsSync(Generator.test), "Test file must not exist");
			assert.ok(!fs.existsSync(Generator.contract), "Contract file must not exist");
			assert.ok(!fs.existsSync(Generator.deploy), "Contract file must not exist");
			assert.ok(!fs.existsSync(Generator.oneFileSource), "Contract file must not exist");
			assert.ok(!fs.existsSync(Generator.path + "build"), "Directory build must not exist");
			assert.ok(!fs.existsSync(Generator.path + "node_modules"), "Directory node_modules must not exist");
		});
	});

	describe("generate", () => {
	    it("should build Smart Contract Source", () => {
	    	Generator.generate();
		    let contract = fs.readFileSync(Generator.contract).toString().replace(/\r?\n|\r/g, '\n');
		    assert.equal(contract, expectedContract, "Contracts should be equal!");
        });
		it("should fail with no name", () => {
			Session.setName(undefined);
			assert.throws(() => Generator.generate(), "If no name is set an error need to be thrown!");
		});
		it("should fail with no template", () => {
			Session.removeTemplate("EIP-20: ERC-20 Token Standard");
			assert.throws(() => Generator.generate(), "If no template is added to the generatorConfig an error need to be thrown!");
		});

        it("should fail with no matching constructor (wrong amount of variables)", () => {
            let generatorConfig = Session.getGeneratorConfig();
            generatorConfig.templates[0].constructorVariable.pop();
            Session.replaceGeneratorConfig(generatorConfig);
            assert.throws(() => Generator.generate(), "If there is no matching constructor in the generatorConfig an error need to be thrown!");
        });
        it("should fail with no matching constructor (wrong variable type)", () => {
            let generatorConfig = Session.getGeneratorConfig();
            generatorConfig.templates[0].constructorVariable[1] = 42;
            Session.replaceGeneratorConfig(generatorConfig);
            assert.throws(() => Generator.generate(), "If there is no matching constructor in the generatorConfig an error need to be thrown!");
        });
        it("should fail with no matching constructor (wrong variable condition, 3rd variable to small)", () => {
            let generatorConfig = Session.getGeneratorConfig();
            generatorConfig.templates[0].constructorVariable[2] = 0;
            Session.replaceGeneratorConfig(generatorConfig);
            assert.throws(() => Generator.generate(), "If there is no matching constructor in the generatorConfig an error need to be thrown!");
        });

		it("should build Smart Contract tests", () => {
			Generator.generate();
			let test = fs.readFileSync(Generator.test).toString().replace(/\r?\n|\r/g, '\n');
			assert.equal(test, expectedTest, "Tests should be equal!");
		});
		it("should build Smart Contract deployment script", () => {
			Generator.generate();
			let deploy = fs.readFileSync(Generator.deploy).toString().replace(/\r?\n|\r/g, '\n');
			assert.equal(deploy, expectedDeployment, "Deployment scripts should be equal!");
		});
    });

	describe("build", function(){
		this.timeout(60000);
		it("should build binaries", () => {
			let result = Generator.build();
			assert.ok(typeof result === "object", "Result needs to be an object");
			assert.ok(typeof result.log === "string", "Result log output needs to be a string");
			assert.ok(typeof result.executionTime === "number", "Result executionTime output needs to be a number");
			assert.ok(fs.existsSync(Generator.path + "build/contracts/Test.json"), "Smart Contract need to be generated");
		});
	});


	describe("buildOneFileSource", function(){
		this.timeout(50000);
		it("should build one file source ", () => {
			let result = Generator.buildOneFileSource();
			assert.ok(typeof result === "object", "Result needs to be an object");
			assert.ok(typeof result.log === "string", "Result log output needs to be a string");
			assert.ok(typeof result.executionTime === "number", "Result executionTime output needs to be a number");
			assert.ok(fs.existsSync(Generator.oneFileSource), "One file source should exist");
		});
	});

	describe("runTests", function(){
		this.timeout(80000);
		it("should succeed", () => {
			let result = Generator.runTests();
			assert.ok(typeof result === "object", "Result needs to be an object");
			assert.ok(typeof result.log === "string", "Result log output needs to be a string");
			assert.ok(typeof result.executionTime === "number", "Result executionTime output needs to be a number");
		});

		it("should fail", () => {
			fs.writeFileSync(Generator.path + "test/EIP/EIP20/EIP20_Default.js", failTest);
			assert.throws(() => Generator.runTests());
		})
	});

});