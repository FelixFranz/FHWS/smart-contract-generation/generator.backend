const Loader = require(__dirname + "/../library/Loader.js").getLoader();
const Config = Loader.get("config");
const fs = require('fs-extra');

describe('All Tests', function () {

    require(__dirname + "/library/index");
    require(__dirname + "/service/index");

    after(function(){
        this.timeout(30000);
        fs.removeSync(Config.session.location);
    });
});