const Loader = require(__dirname + "/../../../library/Loader.js").getLoader();
const Config = Loader.get("config").api;
const SessionManager = Loader.get("lib", "SessionManager");
const assert = require('assert');
const request = require('request');

describe("/session/:sessionId/availabletemplates", () => {

	let sessionId;
	beforeEach(() => sessionId = SessionManager.create().id)

	describe("handleGet", function (){
        this.timeout(5000);
		it("should return a session", function (done){
			request({
				method: "GET",
				uri: "http://127.0.0.1:" + Config.port + "/session/" + sessionId + "/availabletemplates"
			}, function (error, response) {
				if (!!error) assert.fail("There must not be an error");
				let body = JSON.parse(response.body);
				assert(Array.isArray(body), "response must be an array");
				assert(body.length > 0, "Array must not be empty")
				done();
			});
		});
	});

});