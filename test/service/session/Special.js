const Loader = require(__dirname + "/../../../library/Loader.js").getLoader();
const Config = Loader.get("config").api;
const SessionManager = Loader.get("lib", "SessionManager");
const assert = require('assert');
const request = require('request');

describe("/session/:sessionId", () => {

	let sessionId;
	beforeEach(() => sessionId = SessionManager.create().id)

	describe("handleGet", function (){
		it("should return a session", function (done){
			request({
				method: "GET",
				uri: "http://127.0.0.1:" + Config.port + "/session/" + sessionId
			}, function (error, response) {
				if (!!error) assert.fail("There must not be an error");
				let body = JSON.parse(response.body);
				assert(typeof body === 'object', "response must be an object");
				assert(!!body.name, "A name must exist");
				assert(Array.isArray(body.templates), "An array should be returned");
				assert.equal(body.templates.length, 0, "The array should be empty");
				done();
			});
		});
	});

	describe("handlePut", function(){
		it("should change the name of a smart contract", done => {
			request({
				method: "PUT",
				uri: "http://127.0.0.1:" + Config.port + "/session/" + sessionId,
				body: {name: "MyTestContract", compilerVersion: "0.4.25"},
				json: true
			}, function (error, response) {
				if (!!error) assert.fail("There must not be an error");
				let generatorConfig = SessionManager.get(sessionId).getGeneratorConfig();
                assert(generatorConfig.name, "MyTestContract", "The new name should be returned");
                assert(generatorConfig.compilerVersion, "0.4.25", "The new compilerVersion should be returned");
                done();
			});
		});
	});

	describe("handleDelete", function(){
		it("should delete a session", done => {
			request({
				method: "DELETE",
				uri: "http://127.0.0.1:" + Config.port + "/session/" + sessionId,
				body: JSON.stringify({name: "MyTestContract"})
			}, function (error, response) {
				if (!!error) assert.fail("There must not be an error");
				request({
					method: "GET",
					uri: "http://127.0.0.1:" + Config.port + "/session/" + sessionId
				}, function (error, response) {
					if (!!error) assert.fail("There must not be an error");
					assert.equal(response.statusCode, 500, "Status code must be 500!");
					done();
				});
			});
		});
	});

});