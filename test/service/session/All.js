const Loader = require(__dirname + "/../../../library/Loader.js").getLoader();
const Config = Loader.get("config").api;
const assert = require('assert');
const request = require('request');

describe("/session", () => {

	let generatorConfig = '' +
		'{\n' +
		'  "name": "MyContract",\n' +
		'  "templates": [\n' +
		'    {\n' +
		'      "name": "EIP-20: ERC-20 Token Standard",\n' +
		'      "variant": "Default",\n' +
		'      "constructorVariable": [\n' +
		'        "TestToken",\n' +
		'        "Test",\n' +
		'        42,\n' +
		'        1000\n' +
		'      ]\n' +
		'    }\n' +
		'  ]\n' +
		'}';

	describe("handlePost", function (){
		this.timeout(20000);
		it("should return a session", done => {
			request({
				method: "POST",
				uri: "http://127.0.0.1:" + Config.port + "/session"
			}, function (error, response) {
				if (!!error) assert.fail("There must not be an error");
				let body = JSON.parse(response.body);
				assert(typeof body === 'object', "response must be an object");
				assert(!!body.id, "An id need to be generated");
				assert(body.url.includes("/session/"), "The correct url need to be contained");
				done();
			})
		});
		it("should import a session", done => {
			request({
				method: "POST",
				uri: "http://127.0.0.1:" + Config.port + "/session",
				body: generatorConfig
			}, function (error, response) {
				if (!!error) assert.fail("There must not be an error");
				let body = JSON.parse(response.body);
				assert(typeof body === 'object', "response must be an object");
				assert(!!body.id, "An id need to be generated");
				assert(body.url.includes("/session/"), "The correct url need to be contained");
				done();
			})
		});
	});

});