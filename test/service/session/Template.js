const Loader = require(__dirname + "/../../../library/Loader.js").getLoader();
const Config = Loader.get("config").api;
const SessionManager = Loader.get("lib", "SessionManager");
const assert = require('assert');
const request = require('request');

describe("/session/:sessionId/template", () => {

	let Session;
	beforeEach(() => Session = SessionManager.create());
	let addableTemplate = {name: "EIP-20: ERC-20 Token Standard", variant: "Default", constructorVariable: ["Test", "TestTest", 42, 84]};

	describe("handleGet", function (){
        it("should return no template", function (done){
            request({
                method: "GET",
                uri: "http://127.0.0.1:" + Config.port + "/session/" + Session.id + "/template"
            }, function (error, response) {
                if (!!error) assert.fail("There must not be an error");
                assert.equal(response.statusCode, 200, "The status code must to be 200");
                let body = JSON.parse(response.body);
                assert(Array.isArray(body), "response must be an array");
                assert.equal(body.length, 0, "The array should be empty");
                done();
            });
        });

        it("should return a template", function (done){
        	Session.addTemplate(addableTemplate.name, addableTemplate.variant, addableTemplate.constructorVariable);
            request({
                method: "GET",
                uri: "http://127.0.0.1:" + Config.port + "/session/" + Session.id + "/template"
            }, function (error, response) {
                if (!!error) assert.fail("There must not be an error");
                assert.equal(response.statusCode, 200, "The status code must to be 200");
                let body = JSON.parse(response.body);
                assert(Array.isArray(body), "response must be an array");
                assert.equal(body.length, 1, "The array should contain one element");
                assert.equal(typeof body[0], 'object', "The array should contain an object");
                assert.deepEqual(body[0], addableTemplate, "The object must be the added template");
                done();
            });
        });
	});

	describe("handlePost", function(){
		it("should add a template", done => {
			Session = SessionManager.create();
		    request({
                method: "POST",
                uri: "http://127.0.0.1:" + Config.port + "/session/" + Session.id + "/template",
                body: addableTemplate,
                json: true
			}, function (error, response) {
				if (!!error) assert.fail("There must not be an error");
				assert.equal(response.statusCode, 204, "The status code must to be 204");
				let generatorConfig = Session.getGeneratorConfig();
				assert.equal(generatorConfig.templates.length, 1, "Generator config must have a template");
				assert.deepEqual(generatorConfig.templates[0], addableTemplate, "The correct template must be added")
				done();
			});
		});
	});

	describe("handleDelete", function(){
		it("should delete a session", done => {
            Session.addTemplate(addableTemplate.name, addableTemplate.variant, addableTemplate.constructorVariable);
			request({
				method: "DELETE",
				uri: "http://127.0.0.1:" + Config.port + "/session/" + Session.id + "/template",
				body: addableTemplate,
                json: true
			}, function (error, response) {
				if (!!error) assert.fail("There must not be an error");
                assert.equal(response.statusCode, 204, "The status code must to be 204");
                assert.equal(Session.getGeneratorConfig().templates.length, 0, "The element need to be removed!");
				done();
			});
		});
	});

});