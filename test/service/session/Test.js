const Loader = require(__dirname + "/../../../library/Loader.js").getLoader();
const Config = Loader.get("config").api;
const SessionManager = Loader.get("lib", "SessionManager");
const assert = require('assert');
const request = require('request');

describe("/session/:sessionId/test", () => {

	let Session;
	beforeEach(() => {
		Session = SessionManager.create();
		Session.addTemplate("EIP-20: ERC-20 Token Standard", "Default", ["Test", "TestTest", 42, 84]);
    });

	describe("handleGet", function (){
        this.timeout(120000);
		it("should return a session", function (done){
			request({
				method: "GET",
				uri: "http://127.0.0.1:" + Config.port + "/session/" + Session.id + "/test"
			}, function (error, response) {
				if (!!error) assert.fail("There must not be an error");
                assert.equal(response.statusCode, 200, "The status code must to be 200");
                assert(response.headers["content-type"].includes("application/json"), "content-type must be application/json");
                let body = JSON.parse(response.body);
                assert(body.log.length > 0, "The return object must contain a log file");
                assert.equal(typeof body.executionTime, 'number', "There must be a execution time");
				done();
			});
		});
	});
});