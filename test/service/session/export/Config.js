const Loader = require(__dirname + "/../../../../library/Loader.js").getLoader();
const Config = Loader.get("config").api;
const SessionManager = Loader.get("lib", "SessionManager");
const assert = require('assert');
const request = require('request');

describe("/session/:sessionId/export", () => {

	let Session;
	beforeEach(() => {
		Session = SessionManager.create();
		Session.addTemplate("EIP-20: ERC-20 Token Standard", "Default", ["Test", "TestTest", 42, 84]);
    });
	let expectedExport = {
        "name": "MyContract",
        "compilerVersion": "0.4.23",
        "templates": [
            {
                "name": "EIP-20: ERC-20 Token Standard",
                "variant": "Default",
                "constructorVariable": [
                    "Test",
                    "TestTest",
                    42,
                    84
                ]
            }
        ]
    };

	describe("handleGet", function (){
		it("should return the generatorConfig", function (done){
			request({
				method: "GET",
				uri: "http://127.0.0.1:" + Config.port + "/session/" + Session.id + "/export/config"
			}, function (error, response) {
				if (!!error) assert.fail("There must not be an error");
                assert.equal(response.statusCode, 200, "The status code must to be 200");
                assert(response.headers["content-type"].includes("application/json"), "content-type must be application/json");
                let body = JSON.parse(response.body);
                assert.deepEqual(body, expectedExport, "Expected export differs!");
				done();
			});
		});
	});
});