const Loader = require(__dirname + "/../../../../library/Loader.js").getLoader();
const Config = Loader.get("config").api;
const SessionManager = Loader.get("lib", "SessionManager");
const assert = require('assert');
const request = require('request');

describe("/session/:sessionId/export/binaries", () => {

	let Session;
	beforeEach(() => {
		Session = SessionManager.create();
		Session.addTemplate("EIP-20: ERC-20 Token Standard", "Default", ["Test", "TestTest", 42, 84]);
    });

	describe("handleGet", function (){
		this.timeout(50000);
		it("should return the zipped binaries", function (done){
			request({
				method: "GET",
				uri: "http://127.0.0.1:" + Config.port + "/session/" + Session.id + "/export/binaries"
			}, function (error, response) {
				if (!!error) assert.fail("There must not be an error");
                assert.equal(response.statusCode, 200, "The status code must to be 200");
                assert(response.headers["content-type"].includes("application/json"), "content-type must be application/json");
                assert(response.headers["x-file-name"] !== undefined, "x-contract-name header must be exist");
				done();
			});
		});
	});
});