const Loader = require(__dirname + "/../../../../library/Loader.js").getLoader();
const Config = Loader.get("config").api;
const SessionManager = Loader.get("lib", "SessionManager");
const assert = require('assert');
const request = require('request');

describe("/session/:sessionId/export/onefilesource", () => {

	let Session;
	beforeEach(() => {
		Session = SessionManager.create();
		Session.addTemplate("EIP-20: ERC-20 Token Standard", "Default", ["Test", "TestTest", 42, 84]);
		Session.setName("AnyContract");
    });
	let expectedParts = [
	    "pragma solidity ^0.4.23",
        "contract AnyContract is EIP165_Default, EIP20_Default {"
    ];

	describe("handleGet", function (){
		it("should return a OneFileSource", function (done){
		    this.timeout(40000);
			request({
				method: "GET",
				uri: "http://127.0.0.1:" + Config.port + "/session/" + Session.id + "/export/onefilesource"
			}, function (error, response) {
				if (!!error) assert.fail("There must not be an error");
                assert.equal(response.statusCode, 200, "The status code must to be 200");
                assert(response.headers["content-type"].includes("text/plain"), "content-type must be text/plain");
                expectedParts.forEach(p => assert(response.body.includes(p), "Expected parts must be included"));
				done();
			});
		});
	});
});