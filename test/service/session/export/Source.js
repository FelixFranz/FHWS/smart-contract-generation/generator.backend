const Loader = require(__dirname + "/../../../../library/Loader.js").getLoader();
const Config = Loader.get("config").api;
const SessionManager = Loader.get("lib", "SessionManager");
const assert = require('assert');
const request = require('request');

describe("/session/:sessionId/export/source", () => {

	let Session;
	beforeEach(() => {
		Session = SessionManager.create();
		Session.addTemplate("EIP-20: ERC-20 Token Standard", "Default", ["Test", "TestTest", 42, 84]);
    });

	describe("handleGet", function (){
		this.timeout(10000);
		it("should return the zipped source", function (done){
			request({
				method: "GET",
				uri: "http://127.0.0.1:" + Config.port + "/session/" + Session.id + "/export/source"
			}, function (error, response) {
				if (!!error) assert.fail("There must not be an error");
                assert.equal(response.statusCode, 200, "The status code must to be 200");
                assert.equal(response.headers["content-type"], "application/zip", "content-type must be application/zip");
                assert.equal(response.headers["content-disposition"], 'attachment; filename="Source.zip"', 'content-disposition must be attachment; filename="Source.zip"');
				done();
			});
		});
	});
});