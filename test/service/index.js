const Loader = require(__dirname + "/../../library/Loader.js").getLoader();
const RequestProcessor = Loader.get("lib", "RequestProcessor");

describe("Service", () => {

	before(function(){
		this.timeout(5000);
		RequestProcessor.start()
    });
	after(() => RequestProcessor.stop());

	require(__dirname + "/Dispatcher");
	require(__dirname + "/session/All");
	require(__dirname + "/session/Special");
	require(__dirname + "/session/AvailableTemplates");
	require(__dirname + "/session/Template");
	require(__dirname + "/session/export/Config");
	require(__dirname + "/session/export/OneFileSource");
	require(__dirname + "/session/export/Source");
	require(__dirname + "/session/export/Binaries");
	require(__dirname + "/session/Test");
});