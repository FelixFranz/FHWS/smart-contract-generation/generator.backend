const Loader = require(__dirname + "/../../library/Loader.js").getLoader();
const Config = Loader.get("config").api;
const assert = require('assert');
const request = require('request');

describe("/", () => {

	describe("handleGet", () => {
		it("should return a text", done => {
			request({
				method: "GET",
				uri: "http://127.0.0.1:" + Config.port + "/"
			}, function (error, response, body) {
				if (!!error) assert.fail("There must not be an error");
				assert.equal(response.headers["content-type"], "text/html; charset=utf-8", "The content type need to be text/html");
				assert(response.body.includes("This is the api of the Smart Contract Generator!"), "There does not return correct data!");
				done();
			})
		});
	});

});